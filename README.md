# SpringBoot学习之员工管理系统页面素材

#### 介绍
B站【狂神说Java】之SpringBoot最新教程IDEA版通俗易懂 【P20-P28】前端素材
教学视频：https://bilibili.com/video/BV1PE411i7CV
【页面素材源码来源于网络】

#### 使用说明

1.  直接克隆下载即可
2.  狂神SpringBoot教学视频P20-P28的案例素材

#### 关于我
我的Java学习笔记：https://javabook.shiguangping.com

